#include "main.h"


#ifndef STRUCT_H
#define STRUCT_H
#define QUEUE_SIZE 1
typedef struct playerInfo{
	char id,pM;
	int beers=0,shots=0;
	char lives =3;
	char x,y;
	char b;
};
typedef struct playerInfoNet{
	uint32_t id,pM;
	uint32_t beers=0,shots=0,lives =3;
	uint32_t x,y;
	uint32_t b;
};
typedef struct configStruct{
	int seed;
};
typedef struct gameInfo{
	struct playerInfo gracz1;
	struct playerInfo gracz2;
};
class Client
{
	private:
	int sck;
	char *server = "127.0.0.1";	
	char *protocol = "tcp";
	short service_port = 1234;	

	
	public:
	Client(){};
	void initNet();
	void closeNet();
	int writeMessage(void * message,int size);
	int readMessage(void* message,int size);
};
class Server
{
	private:
		short service_port = 1234;
   		int nSocket, nClientSocket;
   		int nBind, nListen;
   		int nFoo = 1;
   		socklen_t nTmp;
   		struct sockaddr_in stAddr, stClientAddr;
	public:
		Server(){};
		void initNet(char** argv);
		void acceptConnect(char** argv);
		void closeNet();
		int writeMessage(void * message,int size);
		int readMessage(void* message,int size);
};
#endif
