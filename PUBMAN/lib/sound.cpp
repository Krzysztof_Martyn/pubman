#include "sound.h"
ALfloat listenerPos[MAX_SOUND];//={0.0,0.0,4.0};
ALfloat listenerVel[MAX_SOUND];//={0.0,0.0,0.0};
ALfloat	listenerOri[MAX_SOUND];//={0.0,0.0,1.0, 0.0,1.0,0.0};


ALfloat sourcePos[MAX_SOUND][3];//={ -2.0, 0.0, 0.0};
ALfloat sourceVel[MAX_SOUND][3];//={ 0.0, 0.0, 0.0};




ALuint	buffer[NUM_BUFFERS];
ALuint	source[NUM_SOURCES];
ALuint  environment[NUM_ENVIRONMENTS];


ALsizei size,freq;
ALenum 	format;
ALvoid 	*data;
ALboolean albool;
int 	ch;

void soundStart(int i)
{
	alSourcePlay(source[i]);
}
void soundStop(int i)
{
	alSourceStop(source[i]);
}
void soundEnd()
{
	for(int i=0;i<MAX_SOUND;i++)
		alSourceStop(source[i]);
	alutExit();
}
void addSound(int i, const char * name)
{
	alutLoadWAVFile((ALbyte*)name,&format,&data,&size,&freq, &albool);
    alBufferData(buffer[i],format,data,size,freq);
    alutUnloadWAV(format,data,size,freq);
}
void bindSound(ALboolean looping ,int i)
{
	    alSourcef(source[i],AL_PITCH,1.0f);
   		alSourcef(source[i],AL_GAIN,1.0f);
    	alSourcefv(source[i],AL_POSITION,sourcePos[i]);
    	alSourcefv(source[i],AL_VELOCITY,sourceVel[i]);
    	alSourcei(source[i],AL_BUFFER,buffer[i]);
    	alSourcei(source[i],AL_LOOPING,looping);
}
void initSound(void)
{
    alutInit(0, NULL);
	for(int i=0;i<MAX_SOUND;i++)
	{
		listenerPos[i] = 0.0f;
		listenerVel[i] = 0.0f;
		listenerOri[i] = 0.0f;
		for(int j=0;j<3;j++)
		{
			sourcePos[i][j] = 0.0f;
			sourceVel[i][j] = 0.0f;
		}	
	}
    alListenerfv(AL_POSITION,listenerPos);
    alListenerfv(AL_VELOCITY,listenerVel);
    alListenerfv(AL_ORIENTATION,listenerOri);
    
    alGetError(); // clear any error messages
    
    if(alGetError() != AL_NO_ERROR) 
    {
        printf("- Error creating buffers !!\n");
        exit(1);
    }
    else
    {
        printf("init() - No errors yet.\n");

    }
    printf("alGenBuffers ...\n");
    // Generate buffers, or else no sound will happen!
    alGenBuffers(NUM_BUFFERS, buffer);
    printf("alGenBuffers ok\n");
<<<<<<< HEAD
	addSound(0,SOUND_0);
    addSound(1,SOUND_1);
	addSound(2,SOUND_2);
	addSound(3,SOUND_3);
	addSound(4,SOUND_4);
	addSound(5,SOUND_5);
	addSound(6,SOUND_6);
	addSound(7,SOUND_7);
=======
	addSound(0,"./sound/ekran_startowy.wav");
    addSound(1,"./sound/game_over.wav");
	addSound(2,"./sound/new_level.wav");
	addSound(3,"./sound/muzyka_w_tle_1.wav");
	addSound(4,"./sound/muzyka_w_tle_2.wav");
	addSound(5,"./sound/piwo.wav");
	addSound(6,"./sound/death.wav");
	addSound(7,"./sound/bonus_jedzenie.wav");
>>>>>>> 2bf02d9c32beebe5f47ba0e747ef7269efff827b
	
    alGetError(); /* clear error */
    alGenSources(NUM_SOURCES, source);

    if(alGetError() != AL_NO_ERROR) 
    {
        printf("- Error creating sources !!\n");
        exit(2);
    }
    else
    {
        printf("init - no errors after alGenSources\n");
    }
	bindSound(AL_TRUE,0);
	bindSound(AL_FALSE,1);
	bindSound(AL_FALSE,2);
	for(int i =3;i<5;i++)
	{
		bindSound(AL_TRUE,i);
	}
		for(int i =5;i<8;i++)
	{
		bindSound(AL_FALSE,i);
	}
}
