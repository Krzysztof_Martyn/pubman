#include "display.h"

void assignVBOtoAttribute(OBJ *o,char* attributeName, GLuint *bufVBO, int variableSize) {
	GLuint location=o->shaderProgram->getAttribLocation(attributeName); //Pobierz numery slotów dla atrybutu
	glBindBuffer(GL_ARRAY_BUFFER,*bufVBO);  //Uaktywnij uchwyt VBO 
	glEnableVertexAttribArray(location); //W³¹cz u¿ywanie atrybutu o numerze slotu zapisanym w zmiennej location
	glVertexAttribPointer(location,variableSize,GL_FLOAT, GL_FALSE, 0, NULL); //Dane do slotu location maj¹ byæ brane z aktywnego VBO
}

void drawObj(OBJ *o,glm::mat4 &matV) {

	o->shaderProgram->use();
	glm::mat4 matP = glm::mat4(1.f);

	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("P"),1, false, glm::value_ptr(matP));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("V"),1, false, glm::value_ptr(matV));//cout<<"2"<<endl;
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M"),1, false,  glm::value_ptr(o->matM));
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,*(o->getTex(0)));
	
	glBindVertexArray((o->vao));
	assignVBOtoAttribute(o,const_cast<char*> ("texCoords"),o->bufTexCoords,2); 
	assignVBOtoAttribute(o,const_cast<char*> ("vertex"),o->bufVertices,4);
	assignVBOtoAttribute(o,const_cast<char*> ("normal"),o->bufNormals,4);
	//Narysowanie obiektu
	glDrawArrays(GL_TRIANGLES,0,o->vertexCount);
	
	//Posprz¹tanie po sobie (niekonieczne w sumie je¿eli korzystamy z VAO dla ka¿dego rysowanego obiektu)
	glBindVertexArray(0);
}
void drawObjectPlayer(OBJ *o,Player *p, int i,glm::mat4 &matV) {

	o->shaderProgram->use();
	glm::mat4 matP=glm::translate(glm::mat4(1.0f),glm::vec3(0.f, 0.f, 0.f-(i+1)*0.01f));

	
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("P"),1, false, glm::value_ptr(matP));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("V"),1, false, glm::value_ptr(matV));//cout<<"2"<<endl;
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M"),1, false,  glm::value_ptr(o->matM));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M2"),1, false,  glm::value_ptr(o->matM2));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("MR"),1, false,  glm::value_ptr(p->matR));
	glUniform1f(o->shaderProgram->getUniformLocation("a"),p->a);
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,*(o->getTex(0)));
	//Uaktywnienie VAO i tym samym uaktywnienie predefiniowanych w tym VAO powi¹zañ slotów atrybutów z tablicami z danymi
	glBindVertexArray((o->vao));
	assignVBOtoAttribute(o,const_cast<char*> ("texCoords"),o->bufTexCoords,2); 
	assignVBOtoAttribute(o,const_cast<char*> ("vertex"),o->bufVertices,4);
	assignVBOtoAttribute(o,const_cast<char*> ("normal"),o->bufNormals,4);
	//Narysowanie obiektu
	glDrawArrays(GL_TRIANGLES,0,o->vertexCount);
	
	//Posprz¹tanie po sobie (niekonieczne w sumie je¿eli korzystamy z VAO dla ka¿dego rysowanego obiektu)
	glBindVertexArray(0);
}
void calculateMatVP(Matrix &matrix , float &tabScreen)
{
	//Wylicz macierz rzutowania
	matrix.P=glm::mat4(1.0f);//glm::perspective(cameraAngle, (float)windowWidth/(float)windowHeight, 1.0f, 100.0f);
	if(tabScreen<0)
		{
			matrix.V = glm::translate(matrix.V0  ,glm::vec3(0.f, tabScreen*4.5, 0.f));	
		}
	else 
	{
		tabScreen=0;		
		matrix.V= matrix.V0;
	}
}
string strAppendInt(string str, int i)
{
	string tmp; // brzydkie rozwiązanie
	sprintf((char*)tmp.c_str(), "%d", i);
	string str2 = tmp.c_str();
	str.append(str2);
	return str;
}
void DrawText(Text *text)
{
	float sx = 2.0 / glutGet(GLUT_WINDOW_WIDTH);
	float sy = 2.0 / glutGet(GLUT_WINDOW_HEIGHT);
	text->initDisplayText(70);
	text->render_text("PLAYER 1", -1 + 8 * sx, 1-30 * sy, sx * 0.5, sy * 0.5);
	text->render_text("PLAYER 2", 1 - 250 * sx, 1-30 * sy, sx * 0.5, sy * 0.5);
	/*text->initDisplayText(65);
	text->render_text("LIVES", -1 + 8 * sx, 1-75 * sy, sx * 0.5, sy * 0.5);
	text->render_text("LIVES", 1 - 250 * sx, 1-75 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("BEERS ",players[0]->beers).c_str()), -1 + 8 * sx, 1-120 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("BEERS ",players[1]->beers).c_str()), 1 - 250 * sx, 1-120 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("SHOTS ",players[0]->shots).c_str()), -1 + 8 * sx, 1-165 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("SHOTS ",players[1]->shots).c_str()), 1 - 250 * sx, 1-165 * sy, sx * 0.5, sy * 0.5);
*/}

